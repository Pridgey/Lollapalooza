﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;
using System.Data.SQLite;
using System.Configuration;

namespace Lollapalooza
{
	public partial class Form1 : Form
	{
		public string path = ConfigurationManager.AppSettings["SQLitePath"];
		public string cip = ConfigurationManager.AppSettings["cip"];
		public Form1()
		{
			InitializeComponent();

			bool exists = CheckForFile();
			if (exists)
			{
				this.Height = (Screen.PrimaryScreen.WorkingArea.Height);
				this.SetDesktopLocation(Screen.PrimaryScreen.WorkingArea.Width - this.Width, 0);
				textBox1.Enabled = true;
				textBox2.Enabled = true;
				textBox3.Enabled = true;
				button1.Enabled = true;
				listView1.Columns.Add("Account", listView1.Width);
				listView1.Columns.Add("ID", 0);
				listView1.Columns.Add("Pass", 0);
				label2.Text = "";
				UpdateList();
			}
			Screen scr = Screen.PrimaryScreen;
		}

		public void UpdateList()
		{
			listView1.Clear();
			var m_dbConnection = new SQLiteConnection("Data Source=" + path + ";Version=3");
			m_dbConnection.Open();
			string getAccounts = "SELECT * FROM records WHERE id > 1 ORDER BY account ASC";
			SQLiteCommand getAccountsCommand = new SQLiteCommand(getAccounts, m_dbConnection);
			SQLiteDataReader reader = getAccountsCommand.ExecuteReader();
			List<Record> records = new List<Record>();
			while (reader.Read())
			{
				records.Add(new Record() { id = int.Parse(reader["id"].ToString()),
					account = StringCipher.Decrypt(reader["account"].ToString(),cip).Replace("\0",string.Empty),
					user = StringCipher.Decrypt(reader["username"].ToString(),cip).Replace("\0",string.Empty),
					pass = StringCipher.Decrypt(reader["password"].ToString(),cip).Replace("\0",string.Empty)
				});
				
			}
			m_dbConnection.Close();
			int i = 0;
			foreach (Record r in records)
			{
				string[] row = { r.account, r.id.ToString(), r.user, r.pass};
				ListViewItem li = new ListViewItem(row);
				listView1.Items.Add(li);
				if (i % 2 == 0) li.BackColor = Color.FromArgb(206,219,237);



				i++;

			}

		}

		private bool CheckForFile()
		{
			string pass = Prompt.ShowDialog("Password", "Access:");
			string salt = ConfigurationManager.AppSettings["salt"];
			string checker = ConfigurationManager.AppSettings["checker"];

			if (File.Exists(path))
			{
				var m_dbConnection = new SQLiteConnection("Data Source=" + path + ";Version=3");
				m_dbConnection.Open();
				string getCheck = "SELECT * FROM records ORDER BY id ASC LIMIT 1";
				SQLiteCommand getCheckCommand = new SQLiteCommand(getCheck, m_dbConnection);
				SQLiteDataReader reader = getCheckCommand.ExecuteReader();
				Record check = new Record();
				while (reader.Read())
				{
					check.id = int.Parse(reader["id"].ToString());
					check.account = reader["account"].ToString();
					check.user = reader["username"].ToString();
					check.pass = reader["password"].ToString();
				}
				if (pass == StringCipher.Decrypt(check.account,cip).Replace("\0",string.Empty) && salt == StringCipher.Decrypt(check.user,cip).Replace("\0", string.Empty) && checker == StringCipher.Decrypt(check.pass,cip).Replace("\0", string.Empty))
				{
					m_dbConnection.Close();
					return true;
				} else
				{
					m_dbConnection.Close();
					return false;
				}

			} else
			{
				SQLiteConnection.CreateFile(path);
				var m_dbConnection = new SQLiteConnection("Data Source=" + path+";Version=3");
				m_dbConnection.Open();
				string create = "CREATE TABLE records (id INTEGER PRIMARY KEY, account VARCHAR(20) NOT NULL, username VARCHAR(20) NOT NULL, password VARCHAR(20) NOT NULL)";
				SQLiteCommand command = new SQLiteCommand(create, m_dbConnection);
				command.ExecuteNonQuery();
				string insert = $"INSERT INTO records (account, username, password) VALUES ('{StringCipher.Encrypt(pass,cip)}','{StringCipher.Encrypt(salt,cip)}','{StringCipher.Encrypt(checker,cip)}')";
				SQLiteCommand inscommand = new SQLiteCommand(insert, m_dbConnection);
				inscommand.ExecuteNonQuery();
				m_dbConnection.Close();
				return true;
			}

		}

		private void button1_Click(object sender, EventArgs e)
		{

			string account = StringCipher.Encrypt(textBox3.Text, cip);
			string user = StringCipher.Encrypt(textBox1.Text, cip);
			string pass = StringCipher.Encrypt(textBox2.Text, cip);
			var m_dbConnection = new SQLiteConnection("Data Source=" + path + ";Version=3");
			m_dbConnection.Open();
			string insert = $"INSERT INTO records (account, username, password) VALUES ('{account}','{user}','{pass}')";
			SQLiteCommand inscommand = new SQLiteCommand(insert, m_dbConnection);
			inscommand.ExecuteNonQuery();
			m_dbConnection.Close();
			UpdateList();
		}

		private void listView1_MouseClick(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				if (listView1.FocusedItem.Bounds.Contains(e.Location) == true)
				{
					contextMenuStrip1.Show(Cursor.Position);
				}
			}
		}

		private void toolStripMenuItem1_Click(object sender, EventArgs e)
		{
			Clipboard.SetText(listView1.SelectedItems[0].SubItems[2].Text);
			Application.Exit();
		}

		private void toolStripMenuItem2_Click(object sender, EventArgs e)
		{
			Clipboard.SetText(listView1.SelectedItems[0].SubItems[3].Text);
			Application.Exit();
		}

		private void toolStripMenuItem3_Click(object sender, EventArgs e)
		{



		}

		private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
		{

		}

		private void seriouslyToolStripMenuItem_Click(object sender, EventArgs e)
		{
			var m_dbConnection = new SQLiteConnection("Data Source=" + path + ";Version=3");
			m_dbConnection.Open();
			string id = listView1.SelectedItems[0].SubItems[1].Text;
			string insert = $"DELETE FROM records WHERE id = {id}";
			SQLiteCommand inscommand = new SQLiteCommand(insert, m_dbConnection);
			inscommand.ExecuteNonQuery();
			m_dbConnection.Close();

			UpdateList();
		}
	}
}
